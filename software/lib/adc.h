#ifndef adc_h
#define adc_h

// Initialize SPI / GPIO hardware for ADC control
void setup_adcs();

//    Read an ADC channel
//
//    Parameters:
//        chan : the adc channel to read
//            Channels 00..07: RF ADS7028 (RF ADC 0)
//                    08..15: RF ADC088S102 (RF ADC 1)
//                    16..35: Non-RF ADC088S102 (+mux) (NON RF ADC)
//
//   Returns:
//        ADC data Normalised to MSB in bit 15
unsigned read_adc(int chan);

#endif // adc
