#ifndef _I2C_H_
#define _I2C_H_

/*
 * Channel IDs 0x00..0x0F are directly mapped to FPGA outputs, and
 * assumed to be preconfigured with prescaler clock <= 400 kHz.
 */
#define MAC_CHANNEL 0x00 // direct FPGA MAC_SCL, MAC_SDA
#define GPIO_RAW_CHANNEL 0x01 // direct FPGA SCL on GPIO0, SDA on GPIO1
/*
 * Channel IDs 0x10+ are routed via 9544 MUXes.
 */
#define I2C_0_CHANNEL 0x10 // 9544 MUXed I2C_0.SCL, .SDA
#define I2C_1_CHANNEL 0x11 // 9544 MUXed I2C_1.SCL, .SDA
#define I2C_2_CHANNEL 0x12 // 9544 MUXed I2C_2.SCL, .SDA
#define I2C_3_CHANNEL 0x13 // 9544 MUXed I2C_3.SCL, .SDA
#define I2C_0_0_CHANNEL 0x14 // 9544 MUXed I2C_0_0.SCL, .SDA
#define I2C_1_0_CHANNEL 0x15 // 9544 MUXed I2C_1_0.SCL, .SDA
#define I2C_2_0_CHANNEL 0x16 // 9544 MUXed I2C_2_0.SCL, .SDA
#define I2C_4_CHANNEL 0x17 // 9544 MUXed I2C_4.SCL, .SDA

/*
 * Write byte over (possibly multiplexed) I2C bus
 *
 * chan: ID of I2C bus (one of *_CHANNEL IDs above)
 * val: byte to write
 * start: 1 to send start condition
 * stop: 1 to send stop condition
 */
extern int i2c_mux_write( int chan, unsigned char val, int start, int stop );

/*
 * Read byte from (possibly multiplexed) I2C bus
 *
 * chan: ID of I2C bus (one of *_CHANNEL IDs above)
 * ack: 1 to send acknowledge bit
 * stop: 1 to send stop condition
 */
extern unsigned char i2c_mux_read( int chan, int ack, int stop );

/*
 * Set directions of single TCA9534 I/O
 *
 * chan: I2C bus identifier
 * addr: TCA9534 address
 * dir: 8x bitwise directions: 0=input, 1=output
 */
extern int i2c_9534_set_direction_single( int chan, int addr,
					  unsigned char dir );
/*
 * Read inputs from single TCA9534 I/O
 *
 * chan: I2C bus identifier
 * addr: TCA9534 address
 */
extern unsigned char i2c_9534_in_single( int chan, int addr );
/*
 * Write outputs to single TCA9534 I/O
 *
 * chan: I2C bus identifier
 * addr: TCA9534 address
 * val: 8x bitwise output
 */
extern int i2c_9534_out_single( int chan, int addr, unsigned char val );

/*
 * Set directions of GPIO0..GPIO31 bank
 *
 * dir: 32x bitwise directions: 0=input, 1=output
 */
extern int i2c_gpio_set_direction( unsigned int dir );
/*
 * Read inputs of GPIO0..GPIO31 bank
 */
extern unsigned int i2c_gpio_in( void  );
/*
 * Write outputs to GPIO0..GPIO31 bank
 *
 * val: 32x bitwise output
 */
extern int i2c_gpio_out( unsigned int val );

/*
 * Set directions of PG0..PG17 bank
 *
 * dir: 18x bitwise directions: 0=input, 1=output
 */
extern int i2c_pg_set_direction( unsigned int dir );
/*
 * Read inputs of PG0..PG17 bank
 */
extern unsigned int i2c_pg_in( void  );
/*
 * Write outputs to PG0..PG17 bank
 *
 * val: 18x bitwise output
 */
extern int i2c_pg_out( unsigned int val );

/*
 * Set directions of EN0..EN17 bank
 *
 * dir: 18x bitwise directions: 0=input, 1=output
 */
extern int i2c_en_set_direction( unsigned int dir );
/*
 * Read inputs of EN0..EN17 bank
 */
extern unsigned int i2c_en_in( void  );
/*
 * Write outputs to EN0..EN17 bank
 *
 * val: 18x bitwise output
 */
extern int i2c_en_out( unsigned int val );

/*
 * Read byte from 24AA025 serial EEPROM
 *
 * addr: byte address
 */
extern unsigned char i2c_ee_read( unsigned addr );
/*
 * Read byte from 24AA025 serial EEPROM
 *
 * addr: byte address
 * val: value to write
 */
extern int i2c_ee_write( unsigned addr, unsigned char val );
/*
 * Poll status of 24AA025 serial EEPROM
 */
extern int i2c_ee_poll( void );
/*
 * Read 48-bit EUI of 24AA025 serial EEPROM
 *
 * buf: caller provided (6 byte) buffer
 */
extern int i2c_ee_read_eui48( unsigned char *buf );

#endif
