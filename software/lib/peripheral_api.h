#ifndef peripheral_api_h
#define peripheral_api_h

#include <stdint.h>

//    Set the direction of a specific gpio pin
//
//    Parameters:
//        gpio : gpio pin
//        dir : direction, 0 -> input, 1 -> output
//
//    Returns:
//        Nothing
void gpio_set_dir(int gpio, int dir);

//    Set the output level of a specific gpio pin
//    Has no effect if the pin is configured as an input
//
//    Parameters:
//        gpio : gpio pin
//        val : output level, 0 -> low, 1 -> high
//
//    Returns:
//        Nothing
void gpio_write(int gpio, int val);

//    Read the level of a specific gpio pin
//
//    Parameters:
//        gpio : gpio pin
//
//    Returns:
//        0 if logic low
//        1 if logic high
int gpio_read(int gpio);

// I2C example read two bytes from i2c device at address 0x10, register 0x03
// i2c_setup(channel, 0);
// i2c_write(channel, (0x10 << 1), 1, 0); // send start condition, address 0x10, write bit
// i2c_write(channel, 0x03, 0, 0); // send 0x03
// i2c_write(channel, (0x10 << 1) | 1, 1, 0); // send repeated start, address 0x10, read bit
// uint8_t byte1 = i2c_read(channel, 1, 0); // read byte, acknowledge
// uint8_t byte2 = i2c_read(channel, 0, 1); // read byte, send stop condition

//    Setup an i2c controller
//
//    Parameters:
//        channel : the specific i2c controller
//        prescaler : the clock prescaler to use. 0 to 7, Frequency is cpu_clock / 2^(prescaler + 1)
//
//    Returns:
//        Nothing
void i2c_setup(int channel, int prescaler);

//    Write a byte over the i2c bus
//
//    Parameters:
//        channel : the i2c bus to use
//        byte : the byte to write
//        send_start : 1 to send start condition, 0 to not
//        send_stop : 1 to send stop condition, 0 to not
//
//    Returns:
//        0 if nack
//        1 if ack
int i2c_write(int channel, uint8_t byte, int send_start, int send_stop);

//    Read a byte from the i2c bus
//
//    Parameters:
//        channel : the i2c bus to use
//        send_ack : 1 to send acknoledge bit, 0 to not
//        send_stop : 1 to send stop contition, 0 to not
//
//    Returns:
//        the byte read
uint8_t i2c_read(int channel, int send_ack, int send_stop);

//    Setup an spi controller
//
//    Parameters:
//        channel : the specific spi controller
//        prescaler : the clock prescaler to use. 0 to 7, Frequency is cpu_clock / 2^(prescaler + 1)
//        cpol : set the spi clock polarity, for details see https://en.wikipedia.org/wiki/Serial_Peripheral_Interface#Clock_polarity_and_phase
//        cpha : set the spi clock phase, for details see https://en.wikipedia.org/wiki/Serial_Peripheral_Interface#Clock_polarity_and_phase 
//        word_size : the word size in bytes for each transfer, 0 for 1 byte, 1 for 2 bytes, 2 for 3 bytes, 3 for 4 bytes
//
//    Returns:
//        Nothing
void spi_setup(int channel, int prescaler, int cpol, int cpha, int word_size);

//    Set cs level
//
//    Parameters:
//        channel : the spi bus to use
//        cs : 0 for low, 1 for high
//
//    Returns:
//        Nothing
void spi_set_cs(int channel, int cs);

//    Write a word to the spi bus (non blocking)
//    After word transfer completes, the response word is available in the rx buffer
//
//    Parameters:
//        channel : the spi bus to use
//        data : the word to write
//
//    Returns:
//        Nothing
void spi_write(int channel, uint32_t data);

//    Read a word a word the spi bus
//
//    Parameters:
//        channel : the spi bus to use
//
//    Returns:
//        the word read
uint32_t spi_read(int channel);

//    Check if words are available in the rx buffer
//
//    Parameters:
//        channel : the spi bus to use
//
//    Returns:
//        1 if data available, 0 otherwise
int spi_rx_avail(int channel);

//    Check if the tx buffer is half full
//
//    Parameters:
//        channel : the spi bus to use
//
//    Returns:
//        1 if tx buffer is half full, 0 otherwise
int spi_tx_half_full(int channel);

//    Check if the spi controller is busy
//
//    Parameters:
//        channel : the spi bus to use
//
//    Returns:
//        1 if spi bus is busy, 0 otherwise
int spi_busy(int channel);

#endif // peripheral_api
