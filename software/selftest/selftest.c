#include <neorv32.h>
#include <string.h>
#include <stdbool.h>

#include "wb_peripherals.h"
#include "i2c.h"
#include "peripheral_api.h"
#include "adc.h"

#define BAUD_RATE 19200

static unsigned cycles( void ) {

    unsigned long ctr;
    
    asm volatile( "rdcycle %0" : "=r" (ctr) );
    
    return ctr;
}

static void delay( int period ) {

    asm volatile( "1:\n\t"
		  "addi %0, %0, -1\n\t"
		  "bnez %0, 1b\n\t"
		  : "=r" ( period )
		  : "0" ( period ) );
}

static char hexdigit( int n ) {

    if( n < 10 )
	return '0' + n;
    else
	return 'A' + n - 10;
}

static int putchar( int c ) {

    neorv32_uart0_putc( c );

    return c;
}

static void show( const char *str ) {

    while( *str )
	putchar( *str++ );
}

static void bs( void ) {

    putchar( '\x08' );
}

static void cr( void ) {

    putchar( '\r' );
}

static void newline( void ) {

    putchar( '\r' );
    putchar( '\n' );
}

static int puts( const char *str ) {

    show( str );
    newline();

    return 0;
}

extern void showhex( unsigned char c ) {

    putchar( hexdigit( c >> 4 ) );
    putchar( hexdigit( c & 0x0F ) );
}

extern void showdec( int n ) {

    int dig;

    dig = 1;
    while( dig * 10 <= n )
	dig *= 10;

    while( dig ) {
	int val = n / dig;
	
	putchar( '0' + val );
	
	n -= val * dig;
	dig /= 10;
    }
}

extern void divider( void ) {

    int i;

    for( i = 0; i < 72; i++ )
	putchar( '-' );

    newline();
}

static int num_tests;
static int num_passed;
static char *section_name;
static int failure;

static void start_section( char *name ) {

    newline();
    divider();
    show( "Testing " );
    puts( section_name = name );
    divider();

    num_tests = 0;
    num_passed = 0;
}

static void result( int code ) {

    num_tests++;

    if( !code )
	num_passed++;
}

static void end_section( void ) {

    newline();

    show( "Summary for " );
    show( section_name );
    show( ": " );
    showdec( num_passed );
    show( " of " );
    showdec( num_tests );
    puts( " test(s) passed." );
    newline();

    if( num_passed != num_tests ) {
	divider();
	show( "*** " );
	showdec( num_tests - num_passed );
	show( " OF " );
	showdec( num_tests );
	putchar( ' ' );
	show( section_name );
	puts( " TEST(S) FAILED!!! *** " );	
	divider();

	failure++;
    }
}

static int recurse( int n ) {

    volatile int saved = n;
    
    int child = n ? recurse( n - 1 ) : 0;
    
    return child + saved;
}

static int test_stack( void ) {

    int result;
    
    show( "Testing stack: " );

    result = recurse( 5 ) != 15;
    
    puts( !result ? "pass" : "fail" );

    return result;
}

static int test_init_data( void ) {

    static volatile int data[ 4 ] = { 0x5A0F1C73, 0xDCD68DB0,
				      0x07462B36, 0x537FBC0B };
    int i;
    int sum = 0;
    int result;
    
    show( "Testing initialised data: " );

    for( i = 0; i < 4; i++ )
	data[ i ] ^= 0x54FEB5CD;

    for( i = 0; i < 4; i++ )
	sum += data[ i ];

    result = sum != 0xF2538AFC;
    
    puts( !result ? "pass" : "fail" );
    
    for( i = 0; i < 4; i++ )
	/* restore original state, in case of subsequent tests */
	data[ i ] ^= 0x54FEB5CD;

    return result;
}

static unsigned int rand_state = 1;

static int _rand( void ) {

    
    /* Shift indices 1, 3, 10 taken from:

       Xorshift RNGs, Marsaglia 2003 (Journal of Statistical Software, 8(14) */

    rand_state ^= rand_state << 1;
    rand_state ^= rand_state >> 3;
    rand_state ^= rand_state << 10;

    return rand_state & 0x7FFFFFFF;
}

static void _srand( unsigned int seed ) {

    rand_state = seed ? seed : 1;
}

static int eeprom_write( void ) {

    int i;

    for( i = 0; i < 8; i++ ) {
	if( i2c_ee_write( i, _rand() & 0xFF ) ) {
	    puts( "EEPROM write failure!" );
	    return -1;
	}

	while( i2c_ee_poll() )
	    ;
    }
    
    return 0;
}

static int eeprom_read( void ) {

    int i;
    
    for( i = 0; i < 8; i++ ) {
	int expected = _rand() & 0xFF;
	int got;
	
	while( i2c_ee_poll() )
	    ;

	got = i2c_ee_read( i );
	
	if( got != expected ) {
	    show( "EEPROM read mismatch: address " );
	    showhex( i );
	    show( ", got " );
	    showhex( got );
	    show( ", expected " );
	    showhex( expected );
	    puts( "!" );
	    
	    return -1;
	}
    }
    
    return 0;
}

static int test_eeprom_rw( void ) {

    show( "Testing EEPROM read/write: " );
    
    _srand( 0x01234567 );
    if( eeprom_write() )
	return -1;
    _srand( 0x01234567 );
    if( eeprom_read() )
	return -1;
    
    _srand( 0xFEDCBA98 );
    if( eeprom_write() )
	return -1;
    _srand( 0xFEDCBA98 );
    if( eeprom_read() )
	return -1;

    puts( "pass" );
    
    return 0;
}

static int test_eeprom_mac( void ) {

    int res;
    unsigned char addr[ 6 ];
    int i;
    
    res = i2c_ee_read_eui48( addr );

    if( res )
	return -1;

    show( "@a" );
    for( i = 0; i < 6; i++ )
	showhex( addr[ i ] );
    newline();

    show( "EEPROM MAC address: " );
    for( i = 0; i < 6; i++ ) {
	if( i )
	    putchar( ':' );
	showhex( addr[ i ] );
    }
    newline();

    return 0;
}

static int test_adc( void ) {

    int i;

    for( i = 0; i < 36; i++ ) {
	unsigned val;
	
	show( "Reading ADC channel " );
	showdec( i );
	show( ": " );
	val = read_adc( i );
	showhex( ( val >> 8 ) & 0xFF );
	showhex( val & 0xFF );
	newline();
    }

    return 0;
}

static int test_i2c_gpio( void ) {

    show( "Testing GPIO 9534: " );

    if( i2c_gpio_out( 0 ) ) {
	puts( "I2C I/O failure!" );
	return -1;
    }

    puts( "pass" );
    return 0;
}

static int test_i2c_pg( void ) {

    show( "Testing PG 9534: " );

    if( i2c_pg_out( 0 ) ) {
	puts( "I2C I/O failure!" );
	return -1;
    }

    puts( "pass" );
    return 0;
}

static int test_i2c_en( void ) {

    show( "Testing EN 9534: " );

    if( i2c_en_out( 0 ) ) {
	puts( "I2C I/O failure!" );
	return -1;
    }

    puts( "pass" );
    return 0;
}

#define SECTOR_SIZE 0x200

#define SD_CS 0x04000000
#define SD_MOSI 0x00400000
#define SD_SCLK 0x00200000
#define SD_MISO 0x00800000
#define SD_DET_A 0x08000000
#define SD_DET_B 0x10000000

#define SD WB_GPIO[ 2 ]

static int sd_readcyc( void ) {

    SD.DOUT = SD_MOSI;
    SD.DOUT = SD_MOSI | SD_SCLK;

    return ( SD.DIN >> 23 ) & 1;
}

static void sd_cmd( const unsigned char *cmd, unsigned char *resp, int len ) {

    int i, j, n;
    
    SD.DOUT = SD_CS; /* /CS high */
    SD.DOUT = 0; /* /CS low */
    
    for( i = 0; i < 6; i++ ) {
	char c = cmd[ i ];
	
	for( j = 0; j < 8; j++ ) {
	    SD.DOUT = c & 0x80 ? SD_MOSI : 0;
	    SD.DOUT = c & 0x80 ? ( SD_MOSI | SD_SCLK ) : SD_SCLK;
	    c <<= 1;
	}
    }

    for( i = 0; i < 0x80; i++ )
	if( !( n = sd_readcyc() ) )
	    break;

    for( i = 0; i < len; i++ ) {
	resp[ i ] = 0;
	
	for( j = 0; j < 8; j++ ) {
	    resp[ i ] = ( resp[ i ] << 1 ) | n;
	    n = sd_readcyc();
	}
    }    
}

static void sd_readrawblock( unsigned char *block ) {

    int i, j;
    
    for( i = 0; i < 0x10000; i++ )
	if( !sd_readcyc() )
	    break;

    for( i = 0; i < SECTOR_SIZE; i++ ) {
	block[ i ] = 0;
	
	for( j = 0; j < 8; j++ )
	    block[ i ] = ( block[ i ] << 1 ) | sd_readcyc();
    }    
}

static void sd_readblock( int addr, unsigned char *block ) {

    unsigned char cmd17[ 6 ];
    unsigned char resp;
    
    cmd17[ 0 ] = 0x51;
    cmd17[ 1 ] = addr >> 24;
    cmd17[ 2 ] = addr >> 16;
    cmd17[ 3 ] = addr >> 8;
    cmd17[ 4 ] = addr;
    cmd17[ 5 ] = 0xFF;
    sd_cmd( cmd17, &resp, 1 );
    sd_readrawblock( block );
}

static int test_sdcard( void ) {

    unsigned char resp[ 5 ], block[ SECTOR_SIZE ];
    int i;
    /* GO_IDLE_STATE */
    static const unsigned char cmd0[] = { 0x40, 0x00, 0x00, 0x00,
					  0x00, 0x95 },
	/* SEND_IF_COND */
	cmd8[] = { 0x48, 0x00, 0x00, 0x01, 0xAA, 0x87 },
	/* READ_OCR */
	cmd58[] = { 0x7A, 0x00, 0x00, 0x00, 0x00, 0xFF },
	/* APP_CMD */
	cmd55[] = { 0x77, 0x00, 0x00, 0x00, 0x00, 0xFF },
	/* SD_SEND_OP_COND */
	acmd41[] = { 0x69, 0x50, 0x30, 0x00, 0x00, 0xFF },
	/* SEND_CID */
	cmd10[] = { 0x4A, 0x00, 0x00, 0x00, 0x00, 0xFF },
	/* SEND_CSD */
	cmd9[] = { 0x49, 0x00, 0x00, 0x00, 0x00, 0xFF };
    char oid[ 2 ];
    char pnm[ 5 ];
    char prv;
    int psn;
    int month, year;
    int size;
    
    show( "Testing SD card: " );

    SD.DIR = SD_CS | SD_MOSI | SD_SCLK | SD_DET_A;

    SD.DOUT = 0;
    if( SD.DIN & SD_DET_B ) {
	puts( "not present (DET_A low)" );

	return 1;
    }
    
    SD.DOUT = SD_DET_A;
    if( !( SD.DIN & SD_DET_B ) ) {
	puts( "not present (DET_A high)" );

	return 1;
    }
    
    /* reset in SPI mode */
    for( i = 0; i < 0x100; i++ ) {
	SD.DOUT = SD_MOSI | SD_CS;
	SD.DOUT = SD_MOSI | SD_CS | SD_SCLK;
    }

    sd_cmd( cmd0, resp, 1 );

    if( resp[ 0 ] != 0x01 ) {
	puts( "fail (reset)" );

	return 1;
    }

    sd_cmd( cmd8, resp, 5 );

    if( !( resp[ 3 ] & 0x01 ) || resp[ 4 ] != 0xAA ) {
	/* not 2V7..3V3 */
	puts( "fail (2.7-3.3 V)" );

	return 1;
    }

    sd_cmd( cmd58, resp, 5 );

    if( ( resp[ 2 ] & 0x30 ) != 0x30 ) {
	puts( "fail (3.2-3.4 V)" );

	return 1;
    }

    for( i = 0; i < 0x1000; i++ ) {
	sd_cmd( cmd55, resp, 1 );
	sd_cmd( acmd41, resp, 1 );

	if( !( resp[ 0 ] & 0x01 ) )
	    break;
    }

    sd_cmd( cmd58, resp, 5 );

    if( ( resp[ 2 ] & 0x30 ) != 0x30 ) {
	puts( "fail (3.2-3.4 V)" );

	return 1;
    }

    if( !( resp[ 1 ] & 0x80 ) ) {
	puts( "fail (power up)" );

	return 1;
    }

    if( !( resp[ 1 ] & 0x40 ) ) {
	puts( "pass (SDSC)" );

	return 0;
    }

    sd_cmd( cmd10, resp, 1 );
    sd_readrawblock( block );

    memcpy( oid, block + 1, 2 );
    memcpy( pnm, block + 3, 5 );
    prv = block[ 8 ];
    psn = ( block[ 9 ] << 24 ) | ( block[ 10 ] << 16 ) | ( block[ 11 ] << 8 ) |
	block[ 12 ];
    year = ( block[ 13 ] << 4 ) + ( block[ 14 ] >> 4 ) + 2000;
    month = block[ 14 ] & 0x0F;

    sd_cmd( cmd9, resp, 1 );
    sd_readrawblock( block );

    size = ( block[ 7 ] << 16 ) | ( block[ 8 ] << 8 ) | block[ 9 ];
    size = ( size + 1 ) >> 1;

    show( "pass (detected " );
    putchar( oid[ 0 ] );
    putchar( oid[ 1 ] );
    putchar( ' ' );
    for( i = 0; i < 5; i++ )
	putchar( pnm[ i ] );
    show( " r" );
    showdec( prv >> 4 );
    putchar( '.' );
    showdec( prv & 0x0F );
    show( " ser " );
    showhex( psn >> 24 );
    showhex( psn >> 16 );
    showhex( psn >> 8 );
    showhex( psn );
    putchar( ' ' );
    showdec( year );
    putchar( '-' );
    showdec( month );
    show( ", " );
    showdec( size );
    puts( " MB)" );

    return 0;
}

extern int main( void ) {

    int i;
    
    neorv32_rte_setup();

    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    puts( "@s" );
    
    start_section( "CPU" );
    result( test_init_data() );
    result( test_stack() );
    end_section();

    for( i = 0; i < WB_I2C_COUNT; i++ )
	i2c_setup( i, 6 );
    
    start_section( "EEPROM" );
    result( test_eeprom_rw() );
    result( test_eeprom_mac() );
    end_section();

    start_section( "ADC" );
    setup_adcs();
    result( test_adc() );
    end_section();

    start_section( "I2C GPIO" );
    result( test_i2c_gpio() );
    result( test_i2c_pg() );
    result( test_i2c_en() );
    end_section();

    start_section( "SD card" );
    result( test_sdcard() );
    end_section();
    
    newline();
    divider();
    divider();
    show( "RESULT: " );
    if( failure ) {
	showdec( failure );
	puts( " FAILURES!!!" );
    } else {
	puts( "ALL TESTS PASSED" );
    }
    divider();
    divider();
    
    puts( "@x" );

    return 0;
}
