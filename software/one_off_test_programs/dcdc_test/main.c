#include <neorv32.h>
#include <string.h>

#include "wb_peripherals.h"
#include "i2c.h"
#include "peripheral_api.h"

#define BAUD_RATE 19200

#define ROSC_GPIO_CHANNEL I2C_3_CHANNEL
#define ROSC_GPIO_ADDR (0x25 << 1)

#define FPGA_SW_PINS ((1 << 0) | (1 << 2))
#define EXTERNAL_SW_PINS ((1 << 1) | (1 << 3))
#define EXTERNAL_CLOCK_ENABLE (1 << 4)

#define DEBUG_PIN 137

enum ROSC_SOURCE_enum {
    ROSC_NONE = 0,
    ROSC_FPGA = 1,
    ROSC_EXTERNAL = 2
};

//    Set the ROSC source
//
//    Parameters:
//        rosc_source : which rosc source to use
//            ROSC_NONE -> use the internal regulator clock
//            ROSC_FPGA -> use a 1.2 MHz clock generated by the FPGA
//            ROSC_EXTERNAL -> use a 1.2 MHz clock generated by the external oscillator
//
//    Returns:
//        Nothing
void set_rosc_source(int rosc_source) {
    static int cur_source = ROSC_NONE;
    if(cur_source == rosc_source) return;

    i2c_9534_set_direction_single(ROSC_GPIO_CHANNEL, ROSC_GPIO_ADDR, 0x1F);
    if(rosc_source == ROSC_FPGA) {
        i2c_9534_out_single(ROSC_GPIO_CHANNEL, ROSC_GPIO_ADDR, FPGA_SW_PINS);
    }
    else if(rosc_source == ROSC_EXTERNAL) {
        if(cur_source == ROSC_FPGA) {
            i2c_9534_out_single(ROSC_GPIO_CHANNEL, ROSC_GPIO_ADDR, FPGA_SW_PINS | EXTERNAL_CLOCK_ENABLE);
        }
        else {
            i2c_9534_out_single(ROSC_GPIO_CHANNEL, ROSC_GPIO_ADDR, 0x00 | EXTERNAL_CLOCK_ENABLE);
        }

        neorv32_cpu_delay_ms(3); // Crystal warmup time max

        i2c_9534_out_single(ROSC_GPIO_CHANNEL, ROSC_GPIO_ADDR, EXTERNAL_SW_PINS | EXTERNAL_CLOCK_ENABLE);
    }

    cur_source = rosc_source;
}

int main() {
    neorv32_rte_setup();
    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    i2c_setup(GPIO_RAW_CHANNEL, 6);

    gpio_set_dir(DEBUG_PIN, 1);

    while(1) {
        gpio_write(DEBUG_PIN, 1);
        set_rosc_source(ROSC_FPGA);
        neorv32_cpu_delay_ms(1000);

        gpio_write(DEBUG_PIN, 0);
        set_rosc_source(ROSC_EXTERNAL);
        neorv32_cpu_delay_ms(1000);
    }

    return 0;
}
