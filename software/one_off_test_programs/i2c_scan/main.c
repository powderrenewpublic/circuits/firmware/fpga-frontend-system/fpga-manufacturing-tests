#include <neorv32.h>
#include <string.h>
#include <stdbool.h>

#include "wb_peripherals.h"
#include "i2c.h"
#include "peripheral_api.h"

#define BAUD_RATE 19200

int channel_ids[13] = {0, 1, 2, 3, 4, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17};
int main() {
    neorv32_rte_setup();

    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    for(int i = 0; i < WB_I2C_COUNT; i++) {
        i2c_setup(i, 6);
    }

    while(1) {
        for(int i = 0; i < 13; i++) {
            for(int j = 0; j < 128; j++) {
                int status = i2c_mux_write(channel_ids[i], (j << 1), 1, 1);
                if(status == 1) {
                    neorv32_uart0_printf("Ack on %d, at %x\r\n", i, j);
                }
                else if(status == -1) {
                    neorv32_uart0_printf("Error on %d, at %x\r\n", i, j);
                }
            }

            neorv32_uart0_print("\r\n");
            neorv32_cpu_delay_ms(1000);
        }
    }

    return 0;
}
