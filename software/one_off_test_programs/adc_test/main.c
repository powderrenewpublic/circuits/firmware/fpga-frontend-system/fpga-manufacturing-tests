#include <string.h>
#include <stdbool.h>

#include "neorv32.h"
#include "peripheral_api.h"
#include "adc.h"

#define BAUD_RATE 19200

#define DEBUG_PIN 138

int main() {
    neorv32_rte_setup();

    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    setup_adcs();

    gpio_set_dir(DEBUG_PIN, 1);

    while(1) {
        for(int i = 16; i < 36; i++) {
            int adc_val = read_adc(i);
            neorv32_uart0_printf("%d: %x\r\n", i + 100, adc_val);
        }

        //int adc_val_15 = read_adc(31);
        //int adc_val_7 = read_adc(23);

        //neorv32_uart0_printf("15: %x\r\n", adc_val_15);
        //neorv32_uart0_printf(" 7: %x\r\n", adc_val_7);
        neorv32_uart0_print("\r\n");

        gpio_write(DEBUG_PIN, 0);
        neorv32_cpu_delay_ms(1000);
        gpio_write(DEBUG_PIN, 1);
    }

    return 0;
}
