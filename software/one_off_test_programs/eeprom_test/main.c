#include <string.h>
#include <stdbool.h>

#include "neorv32.h"
#include "peripheral_api.h"
#include "i2c.h"

#define BAUD_RATE 19200

void write_read_check(uint8_t addr, uint8_t val) {
    int status;
    status = i2c_ee_write(addr, val);

    while(i2c_ee_poll());

    uint8_t read_byte = i2c_ee_read(addr);

    neorv32_uart0_printf("Write status: %d\r\n", status);
    neorv32_uart0_printf("Read byte: %d\r\n", read_byte);
}

int main() {
    uint8_t mac[6];
    int status;

    neorv32_rte_setup();

    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    i2c_setup(MAC_CHANNEL, 4);

    while(1) {
        status = i2c_ee_read_eui48(mac);
        neorv32_uart0_printf("status: %d\r\n", status);

        if(status == 0)

        for(int i = 0; i < 6; i++) {
            neorv32_uart0_printf("%d ", mac[i]);
        }
        neorv32_uart0_print("\r\n");

        write_read_check(0x01, 0x3B);
        neorv32_cpu_delay_ms(1000);
    }

    return 0;
}
