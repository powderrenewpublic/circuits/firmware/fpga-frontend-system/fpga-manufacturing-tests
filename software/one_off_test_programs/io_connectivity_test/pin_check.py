import argparse
from threading import Thread
from queue import Queue, Empty

import serial

NUM_PINS = 384
ctrl = Queue()

def control():
    while True:
        command = input('Enter command: ')
        ctrl.put(command)
        if command == 'x':
            break

t = Thread(target=control)
t.start()

parser = argparse.ArgumentParser(description='Flash the neorv32 firmware')
parser.add_argument('-p', metavar='serial_port')

args = parser.parse_args()

ser = serial.Serial(args.p, 19200, timeout=0.1)
if not ser.isOpen():
    print('opening')
    ser.open()

found_pins = set()

print_all_flag = False

while True:
    try:
        pin = int(ser.readline())
        if not pin in found_pins:
            print(f'Found: {pin}')
            found_pins.add(pin)
        if print_all_flag:
            print(pin)
    except ValueError:
        # Timeout
        pass

    try:
        command = ctrl.get_nowait()

        if command == 'a':
            print(found_pins)
        elif command == 'b':
            print(set(range(NUM_PINS)) - found_pins)
        elif command == 't':
            print_all_flag = not print_all_flag
        elif command == 'x':
            break

    except Empty:
        pass

print(found_pins)
ser.close()
