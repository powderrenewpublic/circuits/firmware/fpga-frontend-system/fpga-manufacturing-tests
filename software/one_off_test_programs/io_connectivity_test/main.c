#include <neorv32.h>
#include <string.h>
#include <stdbool.h>

#include "wb_peripherals.h"

#define BAUD_RATE 19200

uint32_t read_gpio(uint32_t gpio) {
    uint32_t gpio_bit = gpio & 0x1F;
    uint32_t gpio_handle_idx = gpio >> 5;

    return (WB_GPIO[gpio_handle_idx].DIN >> gpio_bit) & 0x1;
}

int main() {
  neorv32_rte_setup();

  neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

  while(1) {
    for(int i = 0; i < 345; i++) {
        uint32_t bit = read_gpio(i);
        bool found = false;
        bool duplicate = false;
        uint32_t probed_bit;
        if(bit == 0) {
            if(!found) {
                found = true;
                probed_bit = i;
            }
            else if(!duplicate) {
                duplicate = true;
            }
        }

        if(found && !duplicate) {
            neorv32_uart0_printf("%d\r\n", probed_bit);
        }
    }
  }

  return 0;
}
