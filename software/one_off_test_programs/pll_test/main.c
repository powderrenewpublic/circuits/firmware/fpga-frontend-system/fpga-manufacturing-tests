#include <neorv32.h>
#include <string.h>
#include <stdbool.h>

#include "wb_peripherals.h"
#include "peripheral_api.h"

#define BAUD_RATE 19200

int main() {
    neorv32_rte_setup();

    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    gpio_set_dir(327, 1);
    gpio_set_dir(139, 1);

    while(1) {
        // Reset PLL
        gpio_write(327, 0);

        // note PLL values are set for syncing to a 40 MHz clk

        WB_SPI[0].CTRL = (1 << SPI_CTRL_EN) | (2 << SPI_CTRL_SIZE0) | (1 << SPI_CTRL_CS0);

        WB_SPI[0].DATA = 0x000004;

        gpio_write(139, 0);
        while(WB_SPI[0].CTRL & (1 << SPI_CTRL_BUSY));
        gpio_write(139, 1);

        WB_SPI[0].CTRL &= ~(1 << SPI_CTRL_CS0);
        WB_SPI[0].CTRL |= (1 << SPI_CTRL_CS0);

        WB_SPI[0].DATA = 0x000001;
        gpio_write(139, 0);
        while(WB_SPI[0].CTRL & (1 << SPI_CTRL_BUSY));
        gpio_write(139, 1);

        WB_SPI[0].CTRL &= ~(1 << SPI_CTRL_CS0);
        WB_SPI[0].CTRL |= (1 << SPI_CTRL_CS0);

        WB_SPI[0].DATA = 0x0D8092;

        gpio_write(139, 0);
        while(WB_SPI[0].CTRL & (1 << SPI_CTRL_BUSY));
        gpio_write(139, 1);

        WB_SPI[0].CTRL &= ~(1 << SPI_CTRL_CS0);
        WB_SPI[0].CTRL |= (1 << SPI_CTRL_CS0);

        gpio_write(327, 1);

        neorv32_uart0_print("PLL configured\r\n");
        neorv32_cpu_delay_ms(1000);
    }

    return 0;
}
