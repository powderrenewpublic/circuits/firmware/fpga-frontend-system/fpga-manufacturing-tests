#include <neorv32.h>
#include <string.h>
#include <stdbool.h>

#include "wb_peripherals.h"
#include "i2c.h"
#include "peripheral_api.h"

#define BAUD_RATE 19200

int gpio_channels[11] = {I2C_3_CHANNEL, I2C_3_CHANNEL, I2C_3_CHANNEL, I2C_3_CHANNEL, I2C_3_CHANNEL, I2C_3_CHANNEL, I2C_4_CHANNEL, I2C_4_CHANNEL, I2C_4_CHANNEL, I2C_4_CHANNEL};
int gpio_addrs[11] = {0x20, 0x21, 0x22, 0x24, 0x25, 0x26, 0x20, 0x22, 0x24, 0x26};

int main() {
    neorv32_rte_setup();

    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    for(int i = 0; i < WB_I2C_COUNT; i++) {
        i2c_setup(i, 6);
    }


    while(1) {
        for(int i = 0; i < 11; i++) {
            for(int j = 0; j < 8; j++) {
                i2c_9534_set_direction_single(gpio_channels[i], gpio_addrs[i]<<1, (1 << j));
                i2c_9534_out_single(gpio_channels[i], gpio_addrs[i]<<1, (1 << j));
                i2c_9534_out_single(gpio_channels[i], gpio_addrs[i]<<1, (0 << j));
                i2c_9534_set_direction_single(gpio_channels[i], gpio_addrs[i]<<1, 0);
            }
        }
    }

    return 0;
}
