#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include "peripheral_api.h"
#include "i2c.h"

#define UART_TX_READY 0x01
#define UART_RX_READY 0x02
extern struct uart {
    volatile unsigned int io;
    volatile unsigned int status;    
} uart;

#define NUM_GPIO 5
#define NUM_GPIO_TEST 4
#define GPIO_MUX 4
extern struct gpio {
    volatile unsigned level[ NUM_GPIO ];
    volatile unsigned direction[ NUM_GPIO ];
} gpio;

extern struct debug {
    volatile unsigned level;
    volatile unsigned direction;
} debug;

extern volatile int led;

#define SECTOR_SIZE 0x200
extern volatile char sd;

extern volatile char flash;

extern volatile int warm_reset;

static unsigned cycles( void ) {

    unsigned long ctr;
    
    asm volatile( "rdcycle %0" : "=r" (ctr) );
    
    return ctr;
}

static void delay( int period ) {

    asm volatile( "1:\n\t"
		  "addi %0, %0, -1\n\t"
		  "bnez %0, 1b\n\t"
		  : "=r" ( period )
		  : "0" ( period ) );
}

static char hexdigit( int n ) {

    if( n < 10 )
	return '0' + n;
    else
	return 'A' + n - 10;
}

extern void putchar( char c ) {

    int retry;

    for( retry = 0; retry < 0x400000; retry++ )
	if( uart.status & UART_TX_READY )
	    break;

    uart.io = c;
}

extern void show( const char *str ) {

    while( *str )
	putchar( *str++ );
}

extern void bs( void ) {

    putchar( '\x08' );
}

extern void cr( void ) {

    putchar( '\r' );
}

extern void newline( void ) {

    putchar( '\r' );
    putchar( '\n' );
}

extern void puts( const char *str ) {

    show( str );
    newline();
}

extern void showhex( unsigned char c ) {

    putchar( hexdigit( c >> 4 ) );
    putchar( hexdigit( c & 0x0F ) );
}

extern void showdec( int n ) {

    int dig;

    dig = 1;
    while( dig * 10 <= n )
	dig *= 10;

    while( dig ) {
	int val = n / dig;
	
	putchar( '0' + val );
	
	n -= val * dig;
	dig /= 10;
    }
}

static int recurse( int n ) {

    volatile int saved = n;
    
    int child = n ? recurse( n - 1 ) : 0;
    
    return child + saved;
}

static int test_stack( void ) {

    int result;
    
    show( "Testing stack: " );

    result = recurse( 5 ) != 15;
    
    puts( !result ? "pass" : "fail" );

    return result;
}

static int test_init_data( void ) {

    static volatile int data[ 4 ] = { 0x5A0F1C73, 0xDCD68DB0,
				      0x07462B36, 0x537FBC0B };
    int i;
    int sum = 0;
    int result;
    
    show( "Testing initialised data: " );

    for( i = 0; i < 4; i++ )
	data[ i ] ^= 0x54FEB5CD;

    for( i = 0; i < 4; i++ )
	sum += data[ i ];

    result = sum != 0xF2538AFC;
    
    puts( !result ? "pass" : "fail" );
    
    for( i = 0; i < 4; i++ )
	/* restore original state, in case of subsequent tests */
	data[ i ] ^= 0x54FEB5CD;

    return result;
}

static void flash_cmd( unsigned cmd, unsigned addr, int addr_len,
		       int dummy_len,
		       unsigned char *result, int result_len ) {
    int i;
    
    flash = 0x02; /* /CS high */
    flash = 0x00; /* /CS low */

    for( i = 0; i < 8; i++ ) {
	flash = ( ( cmd >> 5 ) & 0x04 ) | 0x00; /* clk low */
	flash = ( ( cmd >> 5 ) & 0x04 ) | 0x01; /* clk high */
	cmd <<= 1;
    }

    addr <<= ( 32 - addr_len );
    for( i = 0; i < addr_len; i++ ) {
	flash = ( ( addr >> 29 ) & 0x04 ) | 0x00; /* clk low */
	flash = ( ( addr >> 29 ) & 0x04 ) | 0x01; /* clk high */
	addr <<= 1;
    }

    for( i = 0; i < dummy_len; i++ ) {
	flash = 0x00;
	flash = 0x01;
    }

    while( result_len ) {
	*result = 0;
	for( i = 0; i < 8; i++ ) {
	    flash = 0x00;
	    *result = ( *result << 1 ) | ( ( flash & 0x08 ) >> 3 );
	    flash = 0x01;
	}
	result++;
	result_len--;
    }

    flash = 0x02;
}

static int test_flash( void ) {

    int i;
    unsigned char buf[ 4 ], id[ 8 ];

    show( "Testing flash: " );
    
    /* read serial flash discoverable parameters */
    flash_cmd( 0x5A, 0, 24, 8, buf, 4 );
    if( buf[ 0 ] != 'S' || buf[ 1 ] != 'F' || buf[ 2 ] != 'D' ||
	buf[ 3 ] != 'P' ) {
	puts( "fail" );
	return 1;
    }

    /* read ID */
    flash_cmd( 0x9F, 0, 0, 0, buf, 3 );

    /* read unique ID */
    flash_cmd( 0x4B, 0, 0, 32, id, 8 );
    
    show( "pass (detected M " );
    showhex( buf[ 0 ] );
    show( " part " );
    showhex( buf[ 1 ] );
    showhex( buf[ 2 ] );
    show( " ser " );
    for( i = 0; i < 8; i++ )
	showhex( id[ i ] );
    puts( ")" );

    return 0;
}

static int test_leds( void ) {

    unsigned char n = 0xFE;
    
    show( "Testing LEDs: " );

    do {
	led = n;
	n = ( n << 1 ) | 0x01;
	delay( 0x80000 );
    } while( n != 0xFF );

    led = 0xFF;

    puts( "done" );

    return 0;
}

static void show_cycles( const char *params ) {

    unsigned c = cycles();

    showhex( c >> 24 );
    showhex( c >> 16 );
    showhex( c >> 8 );
    showhex( c );

    newline();
}

static void spi_start( unsigned char *spi ) {

    *spi = 0x02; /* raise /CS */
    delay( 8 ); /* tSW = 80 ns */
    *spi = 0x00; /* lower /CS */
}

static unsigned char spi_byte( unsigned char *spi, unsigned char out ) {

    unsigned char in = 0;
    int i;

    for( i = 0; i < 8; i++ ) {
	*spi = ( ( out >> 5 ) & 0x04 ) | 0; /* lower SCLK */
	delay( 2 ); /* tDS = 20 ns */
	*spi = ( ( out >> 5 ) & 0x04 ) | 1; /* raise SCLK */
	out <<= 1;
	in = ( in << 1 ) | ( ( *spi & 0x08 ) >> 3 );
	delay( 2 ); /* tDH = 20 ns */
    }

    return in;
}

static void spi_end( unsigned char *spi ) {
    
    *spi = 0x00; /* clock low */
    delay( 5 ); /* tSH = 50 ns */
    *spi = 0x02; /* raise /CS */
}

static void transaction( unsigned char *spi, const unsigned char *in,
			 int len ) {

    spi_start( spi );
    while( len-- )
	spi_byte( spi, *in++ );
    spi_end( spi );
}

static void wait_cts( unsigned char *spi, unsigned char *out, int len ) {

    int i;
    char cts;

    for( i = 0; i < 0x40; i++ ) {
	spi_start( spi );
	spi_byte( spi, 0x44 );
	cts = spi_byte( spi, 0x00 );

	if( cts == 0xFF ) {
	    int j;
		
	    for( j = 0; j < len; j++ )
		*out++ = spi_byte( spi, 0xFF );
	    
	    break;
	}
	
	delay( 0x1000 );
    }

    spi_end( spi );
}

static int check_error( unsigned char *spi ) {

    static const unsigned char get_chip_status[] = {
	0x23, 0x7F
    };
    char cts, err = 0, cmd;
    int i;
    
    transaction( spi, get_chip_status, sizeof get_chip_status );

    for( i = 0; i < 0x40; i++ ) {
	spi_start( spi );
	spi_byte( spi, 0x44 );
	cts = spi_byte( spi, 0x00 );

	if( cts == 0xFF ) {
	    spi_byte( spi, 0xFF );
	    spi_byte( spi, 0xFF );
	    err = spi_byte( spi, 0xFF );
	    cmd = spi_byte( spi, 0xFF );

	    if( err ) {
		putchar( 'E' );
		putchar( ' ' );
		showhex( err );
		putchar( ' ' );
		showhex( cmd );
	    }

	    goto done;
	}

	delay( 0x1000 );
    }

    err = -1; /* timeout */
done:
    spi_end( spi );

    return err;
}

static void reset( const char *param ) {

    warm_reset = 0;
}

static int sd_readcyc( void ) {

    sd = 0x04;
    sd = 0x05;
    
    return ( sd >> 3 ) & 0x01;
}

static void sd_cmd( const unsigned char *cmd, unsigned char *resp, int len ) {

    int i, j, n;
    
    sd = 0x02; /* /CS high */
    sd = 0x00; /* /CS low */
    
    for( i = 0; i < 6; i++ ) {
	char c = cmd[ i ];
	
	for( j = 0; j < 8; j++ ) {
	    sd = c & 0x80 ? 0x04: 0x00;
	    sd = c & 0x80 ? 0x05: 0x01;
	    c <<= 1;
	}
    }

    for( i = 0; i < 0x80; i++ )
	if( !( n = sd_readcyc() ) )
	    break;

    for( i = 0; i < len; i++ ) {
	resp[ i ] = 0;
	
	for( j = 0; j < 8; j++ ) {
	    resp[ i ] = ( resp[ i ] << 1 ) | n;
	    n = sd_readcyc();
	}
    }    
}

static void sd_readrawblock( unsigned char *block ) {

    int i, j;
    
    for( i = 0; i < 0x10000; i++ )
	if( !sd_readcyc() )
	    break;

    for( i = 0; i < SECTOR_SIZE; i++ ) {
	block[ i ] = 0;
	
	for( j = 0; j < 8; j++ )
	    block[ i ] = ( block[ i ] << 1 ) | sd_readcyc();
    }    
}

static void sd_readblock( int addr, unsigned char *block ) {

    unsigned char cmd17[ 6 ];
    unsigned char resp;
    
    cmd17[ 0 ] = 0x51;
    cmd17[ 1 ] = addr >> 24;
    cmd17[ 2 ] = addr >> 16;
    cmd17[ 3 ] = addr >> 8;
    cmd17[ 4 ] = addr;
    cmd17[ 5 ] = 0xFF;
    sd_cmd( cmd17, &resp, 1 );
    sd_readrawblock( block );
}

static int test_sdcard( void ) {

    volatile char *const sd_raw = (volatile char *) 0x08800000;
    unsigned char resp[ 5 ], block[ SECTOR_SIZE ];
    int i;
    /* GO_IDLE_STATE */
    static const unsigned char cmd0[] = { 0x40, 0x00, 0x00, 0x00,
					  0x00, 0x95 },
	/* SEND_IF_COND */
	cmd8[] = { 0x48, 0x00, 0x00, 0x01, 0xAA, 0x87 },
	/* READ_OCR */
	cmd58[] = { 0x7A, 0x00, 0x00, 0x00, 0x00, 0xFF },
	/* APP_CMD */
	cmd55[] = { 0x77, 0x00, 0x00, 0x00, 0x00, 0xFF },
	/* SD_SEND_OP_COND */
	acmd41[] = { 0x69, 0x50, 0x30, 0x00, 0x00, 0xFF },
	/* SEND_CID */
	cmd10[] = { 0x4A, 0x00, 0x00, 0x00, 0x00, 0xFF },
	/* SEND_CSD */
	cmd9[] = { 0x49, 0x00, 0x00, 0x00, 0x00, 0xFF };
    char oid[ 2 ];
    char pnm[ 5 ];
    char prv;
    int psn;
    int month, year;
    int size;
    
    show( "Testing SD card: " );

    showhex( *sd_raw );
    
    if( *sd_raw & 0x10 ) {
	puts( "not present" );

	return 1;
    }
    
    /* reset in SPI mode */
    for( i = 0; i < 0x100; i++ ) {	    
	*sd_raw = 0x06;
	*sd_raw = 0x07;
    }

    sd_cmd( cmd0, resp, 1 );

    if( resp[ 0 ] != 0x01 ) {
	puts( "fail (reset)" );

	return 1;
    }

    sd_cmd( cmd8, resp, 5 );

    if( !( resp[ 3 ] & 0x01 ) || resp[ 4 ] != 0xAA ) {
	/* not 2V7..3V3 */
	puts( "fail (2.7-3.3 V)" );

	return 1;
    }

    sd_cmd( cmd58, resp, 5 );

    if( ( resp[ 2 ] & 0x30 ) != 0x30 ) {
	puts( "fail (3.2-3.4 V)" );

	return 1;
    }

    for( i = 0; i < 0x1000; i++ ) {
	sd_cmd( cmd55, resp, 1 );
	sd_cmd( acmd41, resp, 1 );

	if( !( resp[ 0 ] & 0x01 ) )
	    break;
    }

    sd_cmd( cmd58, resp, 5 );

    if( ( resp[ 2 ] & 0x30 ) != 0x30 ) {
	puts( "fail (3.2-3.4 V)" );

	return 1;
    }

    if( !( resp[ 1 ] & 0x80 ) ) {
	puts( "fail (power up)" );

	return 1;
    }

    if( !( resp[ 1 ] & 0x40 ) ) {
	puts( "pass (SDSC)" );

	return 0;
    }

    sd_cmd( cmd10, resp, 1 );
    sd_readrawblock( block );

    memcpy( oid, block + 1, 2 );
    memcpy( pnm, block + 3, 5 );
    prv = block[ 8 ];
    psn = ( block[ 9 ] << 24 ) | ( block[ 10 ] << 16 ) | ( block[ 11 ] << 8 ) |
	block[ 12 ];
    year = ( block[ 13 ] << 4 ) + ( block[ 14 ] >> 4 ) + 2000;
    month = block[ 14 ] & 0x0F;

    sd_cmd( cmd9, resp, 1 );
    sd_readrawblock( block );

    size = ( block[ 7 ] << 16 ) | ( block[ 8 ] << 8 ) | block[ 9 ];
    size = ( size + 1 ) >> 1;

    show( "pass (detected " );
    putchar( oid[ 0 ] );
    putchar( oid[ 1 ] );
    putchar( ' ' );
    for( i = 0; i < 5; i++ )
	putchar( pnm[ i ] );
    show( " r" );
    showdec( prv >> 4 );
    putchar( '.' );
    showdec( prv & 0x0F );
    show( " ser " );
    showhex( psn >> 24 );
    showhex( psn >> 16 );
    showhex( psn >> 8 );
    showhex( psn );
    putchar( ' ' );
    showdec( year );
    putchar( '-' );
    showdec( month );
    show( ", " );
    showdec( size );
    puts( " MB)" );

    return 0;
}

static unsigned ads7028_cycle( int chan ) {

    static int prev_chan;
    int val;
    
    if( chan != prev_chan ) {
	// FIXME need a dummy cycle to write new control register
#if 0
	cs_low();
	shift_in( 0x08 ); // register write
	shift_in( 0x11 ); // select channel
	shift_in( chan );
	cs_high();
#endif
	
	prev_chan = chan;
    }
    
    // FIXME implement I/O interface to ADS7028 control logic here
#if 0
    cs_low();
    shift_in( 0x08 ); // register write
    shift_in( 0x11 ); // select channel
    val = shift_in( chan );
    cs_high();
#else
    val = 0;
#endif

    return val << 4;
}

static unsigned adc088s102_cycle( int ic, int chan ) {

    static int prev_chan[ 2 ];
    int val;
    
    if( chan != prev_chan[ ic ] ) {
	// FIXME need a dummy cycle to write new control register
#if 0
	cs_low( ic );
	shift_in( ic, chan ); // select channel
	cs_high( ic );
#endif

	prev_chan[ ic ] = chan;
    }
    
    // FIXME implement I/O interface to ADC088S102 control logic here
#if 0
    cs_low( ic );
    val = shift_in( ic, chan ); // select channel
    cs_high( ic );
#else
    val = 0;
#endif
    
    return val << 4;
}

/* Channels 0..7: RF ADS7028
            8..15: RF ADC088S102
	    16..35: Non-RF ADC088S102 (+mux)

   Normalised to MSB in bit 15 */
static unsigned read_adc( int chan ) {

    if( chan < 8 )
	return ads7028_cycle( chan );
    else if( chan < 16 )
	return adc088s102_cycle( 0, chan & 7 );
    else {
	// U1001A S on bit 1, U1004A S on bit 0
	if( chan < 24 )
	    gpio.level[ GPIO_MUX ] = 0x02;
	else if( chan < 32 )
	    gpio.level[ GPIO_MUX ] = 0x00;
	else
	    gpio.level[ GPIO_MUX ] = 0x01;
	    
	return adc088s102_cycle( 1, chan & 7 );
    }
}

static int test_adc( void ) {

    int chan, i;

    gpio.direction[ GPIO_MUX ] = 0x3;
    gpio.level[ GPIO_MUX ] = 0;
    
    for( chan = 0; chan < 36; chan++ )
	for( i = 0; i < 4; i++ ) {
	    unsigned samp = read_adc( chan );
	    
	    if( samp < 0x4000 || samp > 0xC000 ) {
		// FIXME can apply tighter tolerances once channel mappings
		// and expected voltages are defined
		show( "unexpected sample " );
		showhex( samp );
		show( " on ADC channel " );
		showdec( chan );
		newline();
		
		return 1;
	    }
	}

    return 0;
}

static unsigned int rand( void ) {

    static unsigned int state = 1;
    
    /* Shift indices 1, 3, 10 taken from:

       Xorshift RNGs, Marsaglia 2003 (Journal of Statistical Software, 8(14) */

    state ^= state << 1;
    state ^= state >> 3;
    state ^= state << 10;

    return state;
}

static int test_gpio( int master ) {

    int i, trial;
    unsigned int states[ NUM_GPIO_TEST ];

    /* clock/sync signal on debug connector */
    debug.direction = master ? 1 : 0;
    debug.level = 0;
    
    for( i = 0; i < NUM_GPIO_TEST; i++ )
	gpio.direction[ i ] = master ? 0xFFFFFFFF : 0;
    
    for( trial = 0; trial < 0x100; trial++ ) {
	for( i = 0; i < NUM_GPIO_TEST; i++ )
	    states[ i ] = rand();

	if( master ) {
	    for( i = 0; i < NUM_GPIO_TEST; i++ )
		gpio.level[ i ] = states[ i ];
	
	    delay( 1000 );

	    debug.level = 1;
	    
	    delay( 1000 );

	    debug.level = 0;
	} else {
	    while( !debug.level )
		;

	    for( i = 0; i < NUM_GPIO_TEST; i++ )
		if( gpio.level[ i ] != states[ i ] ) {
		    int bit;

		    show( "GPIO mismatch on port " );
		    showdec( i );
		    show( " bit " );

		    for( bit = 0; bit < 32; bit++ )
			if( ( ( gpio.level[ i ] >> bit ) & 1 ) !=
			    ( ( states[ i ] >> bit ) & 1 ) ) {
			    showdec( bit );
			    break;
			}

		    newline();
			    
		    return 1;
		}
	    
	    while( debug.level )
		;
	}
    }

    return 0;
}

static int test( int master ) {

    int result;
    
    result = test_init_data(); // test reading+writing initialised (block) RAM

    result |= test_stack(); // test reading and writing stack

    result |= test_leds();

    result |= test_gpio( master );
    
    result |= test_flash(); // test reading external SPI flash

    result |= test_sdcard(); // test reading SD card

    result |= test_adc(); // test ADC088S102 and ADS7028 conversions
    
    return result;
}

extern int main( void ) {

    extern unsigned char _bss_start, _bss_end;
    unsigned char *p;
    int result;
    
    led = 0xFF;
    
    /* clear BSS */
    for( p = &_bss_start; p < &_bss_end; p++ )
	*p = 0;
    
    // flush UART RX buffer
    if( uart.status & UART_RX_READY )
	(void) uart.io;

    result = test( 0 );

    newline();
    puts( result ? "FAILED!" : "Passed." );

    while( 1 )
	;
}
