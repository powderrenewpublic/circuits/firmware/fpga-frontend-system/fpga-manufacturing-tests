#include "neorv32.h"

#include "peripheral_api.h"

#define BAUD_RATE 19200

int main() {
    neorv32_rte_setup();
    neorv32_uart0_setup(BAUD_RATE, PARITY_NONE, FLOW_CONTROL_NONE);

    gpio_set_dir(39, 1); // LED 0

    while(1) {
        neorv32_uart0_printf("Hello World!\r\n");

        gpio_write(39, 1);
        neorv32_cpu_delay_ms(500);
        gpio_write(39, 0);
        neorv32_cpu_delay_ms(500);
    }

    return 0;
}
