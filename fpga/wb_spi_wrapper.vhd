library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library neorv32;
use neorv32.neorv32_package.all;

entity wb_spi_wrapper is
	port (
        clk : in std_logic;
        rst : in std_logic;

        wb_adr : in std_logic_vector(2 downto 2);
        wb_we : in std_logic;
        wb_stb : in std_logic;
        wb_cyc : in std_logic;
        wb_dat_from_master : in std_logic_vector(31 downto 0);
        wb_dat_to_master : out std_logic_vector(31 downto 0);
        wb_ack : out std_logic;

        clkgen_i : in std_logic_vector(7 downto 0);

        spi_sck_o : out std_logic;
        spi_sdo_o : out std_logic;
        spi_sdi_i : in std_logic;
        spi_csn_o : out std_logic_vector(7 downto 0);

        irq_o : out std_logic
	);
end wb_spi_wrapper;

architecture a1 of wb_spi_wrapper is
	signal rstn : std_logic;
	signal wb_dat_to_master_u : std_ulogic_vector(31 downto 0);
    signal rden : std_logic;
    signal wren : std_logic;

    signal spi_csn_o_u : std_ulogic_vector(7 downto 0);

    signal prev_stb : std_logic;
begin
	rstn <= not rst;
    wb_dat_to_master <= std_logic_vector(wb_dat_to_master_u);
    spi_csn_o <= std_logic_vector(spi_csn_o_u);

    rden <= (not wb_we) and wb_stb and wb_cyc and not prev_stb;
    wren <= wb_we and wb_stb and wb_cyc and not prev_stb;

    stb_pulse : process(clk)
    begin
        if rising_edge(clk) then
            prev_stb <= wb_stb;
        end if;
    end process;

    neorv32_spi_inst : neorv32_spi
    generic map (
        IO_SPI_FIFO => 4
    )
    port map (
        -- host access --
        clk_i => clk,
        rstn_i => rstn,
        addr_i(31 downto 3) => spi_base_c(31 downto 3),
        addr_i(2) => wb_adr(2),
        addr_i(1 downto 0) => spi_base_c(1 downto 0),
        rden_i => rden,
        wren_i => wren,
        data_i => std_ulogic_vector(wb_dat_from_master),
        data_o => wb_dat_to_master_u,
        ack_o => wb_ack,
        -- clock generator --
        clkgen_en_o => open,
        clkgen_i => std_ulogic_vector(clkgen_i),
        -- com lines --
        spi_sck_o => spi_sck_o,
        spi_sdo_o => spi_sdo_o,
        spi_sdi_i => spi_sdi_i,
        spi_csn_o => spi_csn_o_u,
        -- interrupt --
        irq_o => irq_o
    );
end architecture;
