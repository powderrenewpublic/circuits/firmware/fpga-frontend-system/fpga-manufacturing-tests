library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library neorv32;
use neorv32.neorv32_package.all;

use work.fpga_main_package.all;

entity fpga_main_top is
    port (
        Clock_Source_VCXO_OUT : in std_logic;
        Clock_Source_REF_IN : out std_logic;
        EXT_CLK_IN : in std_logic;
        FPGA_DEBUG_GPIO : inout std_logic_vector(13 downto 0);
        ROSC_OUT : out std_logic;
        gpio : inout std_logic_vector(NUM_GPIO_PINS-1 downto 0);

        SPI_SCLK : out std_logic_vector(NUM_SPI_CORES-1 downto 0);
        SPI_MOSI : out std_logic_vector(NUM_SPI_CORES-1 downto 0);
        SPI_MISO : in std_logic_vector(NUM_SPI_CORES-1 downto 0);
        SPI_CS : out std_logic_vector(NUM_SPI_CORES-1 downto 0);

        I2C_SDA : inout std_logic_vector(NUM_I2C_CORES-1 downto 0);
        I2C_SCL : inout std_logic_vector(NUM_I2C_CORES-1 downto 0)
    );
end fpga_main_top;

architecture a1 of fpga_main_top is
    signal con_gpio_o : std_ulogic_vector(63 downto 0);

    signal clk : std_logic;
    signal clk_1_2mhz : std_logic;

    signal rst : std_logic;
    signal rstn : std_logic;

    signal wb_adr_o_u : std_ulogic_vector(31 downto 0);
    signal wb_adr_o : std_logic_vector(31 downto 0);
    signal wb_dat_i : std_logic_vector(31 downto 0);
    signal wb_dat_o_u : std_ulogic_vector(31 downto 0);
    signal wb_dat_o : std_logic_vector(31 downto 0);
    signal wb_we_o : std_logic;
    signal wb_stb_o : std_logic;
    signal wb_cyc_o : std_logic;
    signal wb_ack_i : std_logic;

    signal gpio_dummy : std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);

    component wb_periph_conn
        port (
            wb_rst : in std_logic;
            wb_clk : in std_logic;
            wb_adr : in std_logic_vector(31 downto 0);
            wb_we : in std_logic;
            wb_stb : in std_logic;
            wb_cyc : in std_logic;
            wb_dat_from_master : in std_logic_vector(31 downto 0);
            wb_dat_to_master : out std_logic_vector(31 downto 0);
            wb_ack : out std_logic;

            gpio : inout std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);

            spi_sck : out std_logic_vector(NUM_SPI_CORES-1 downto 0);
            spi_sdo : out std_logic_vector(NUM_SPI_CORES-1 downto 0);
            spi_sdi : in std_logic_vector(NUM_SPI_CORES-1 downto 0);
            spi_csn : out std_logic_vector(NUM_SPI_CORES-1 downto 0);

            i2c_sda : inout std_logic_vector(NUM_I2C_CORES-1 downto 0);
            i2c_scl : inout std_logic_vector(NUM_I2C_CORES-1 downto 0)
        );
    end component;

    component system_pll
        port (CLK: in std_logic; CLKOP: out std_logic; CLKOK: out std_logic;
            LOCK: out std_logic);
    end component;
begin
    clk <= Clock_Source_VCXO_OUT;
    Clock_Source_REF_IN <= EXT_CLK_IN;
    ROSC_OUT <= clk_1_2mhz;

    rstn <= FPGA_DEBUG_GPIO(8);
    rst <= not rstn;

    --FPGA_DEBUG_GPIO(7) <= con_gpio_o(0);

    wb_adr_o <= std_logic_vector(wb_adr_o_u);
    wb_dat_o <= std_logic_vector(wb_dat_o_u);

    neorv32_top_inst : neorv32_top
    generic map (
        CUSTOM_ID                    => x"00000000",
        -- General --
        CLOCK_FREQUENCY              => 40000000,   -- clock frequency of clk_i in Hz
        INT_BOOTLOADER_EN            => true,              -- boot configuration: true = boot explicit bootloader; false = boot from int/ext (I)MEM
        -- RISC-V CPU Extensions --
        CPU_EXTENSION_RISCV_C        => true,              -- implement compressed extension?
        CPU_EXTENSION_RISCV_M        => true,              -- implement mul/div extension?
        CPU_EXTENSION_RISCV_Zicsr    => true,              -- implement CSR system?
        CPU_EXTENSION_RISCV_Zicntr   => true,              -- implement base counters?
        -- Internal Instruction memory --
        MEM_INT_IMEM_EN              => true,              -- implement processor-internal instruction memory
        MEM_INT_IMEM_SIZE            => 16*1024, -- size of processor-internal instruction memory in bytes
        -- Internal Data memory --
        MEM_INT_DMEM_EN              => true,              -- implement processor-internal data memory
        MEM_INT_DMEM_SIZE            => 8*1024, -- size of processor-internal data memory in bytes
        -- Processor peripherals --
        IO_GPIO_EN                   => true,              -- implement general purpose input/output port unit (GPIO)?
        IO_MTIME_EN                  => true,              -- implement machine system timer (MTIME)?
        IO_UART0_EN                  => true,              -- implement primary universal asynchronous receiver/transmitter (UART0)?
        IO_UART0_RX_FIFO             => 4,
        IO_UART0_TX_FIFO             => 4,

        FAST_MUL_EN                  => true,  -- use DSPs for M extension's multiplier

        MEM_EXT_EN                   => true
    )
    port map (
        -- Global control --
        clk_i       => clk,       -- global clock, rising edge
        rstn_i      => rstn,      -- global reset, low-active, async
        -- GPIO (available if IO_GPIO_EN = true) --
        gpio_o      => con_gpio_o,  -- parallel output
        -- primary UART0 (available if IO_UART0_EN = true) --
        uart0_txd_o => FPGA_DEBUG_GPIO(5), -- UART0 send data
        uart0_rxd_i => FPGA_DEBUG_GPIO(4),  -- UART0 receive data

        -- Wishbone bus interface
        wb_tag_o => open, -- request tag
        wb_adr_o => wb_adr_o_u, -- address
        wb_dat_i => std_ulogic_vector(wb_dat_i), -- read data
        wb_dat_o => wb_dat_o_u, -- write data
        wb_we_o => wb_we_o, -- read/write
        wb_sel_o => open, -- byte enable
        wb_stb_o => wb_stb_o, -- strobe
        wb_cyc_o => wb_cyc_o, -- valid cycle
        wb_ack_i => wb_ack_i, -- transfer acknowledge
        wb_err_i => open -- transfer error
    );

    wb_periph_conn_inst : wb_periph_conn
    port map (
        wb_rst => rst,
        wb_clk => clk,
        wb_adr => wb_adr_o,
        wb_we => wb_we_o,
        wb_stb => wb_stb_o,
        wb_cyc => wb_cyc_o,
        wb_dat_from_master => wb_dat_o,
        wb_dat_to_master => wb_dat_i,
        wb_ack => wb_ack_i,

        gpio(NUM_GPIO_PINS-1 downto 0) => gpio,
        gpio(NUM_GPIO_PINS) => FPGA_DEBUG_GPIO(0),
        gpio(NUM_GPIO_CORES*32-1 downto NUM_GPIO_PINS+1) => gpio_dummy(NUM_GPIO_CORES*32-1 downto NUM_GPIO_PINS+1),

        spi_sck => SPI_SCLK,
        spi_sdo => SPI_MOSI,
        spi_sdi => SPI_MISO,
        spi_csn => SPI_CS,

        i2c_sda => I2C_SDA,
        i2c_scl => I2C_SCL
    );

    system_pll_inst : system_pll
    port map (
        CLK => Clock_Source_VCXO_OUT,
        CLKOP => open,
        CLKOK => clk_1_2mhz,
        LOCK => FPGA_DEBUG_GPIO(7)
    );
end architecture;
