library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library neorv32;
use neorv32.neorv32_package.all;

use work.fpga_main_package.all;

entity breiva2_top is
    port (
        ext_reset : in std_logic;
        clk_50mhz : in std_logic;
        -- Note: All switches are debounced except for sw3, see schematic
        sw : in std_logic_vector(7 downto 0);
        exp_io : inout std_logic_vector(39 downto 1);
        led : out std_logic_vector(7 downto 0);
        -- uart
        rx : in std_logic;
        tx : out std_logic
    );
end breiva2_top;

architecture a1 of breiva2_top is
    signal clk : std_logic;

    signal rst : std_logic;
    signal rstn : std_logic;

	signal led_int : std_logic_vector(7 downto 0);
	signal sw_int : std_logic_vector(7 downto 0);

    signal wb_adr_o_u : std_ulogic_vector(31 downto 0);
    signal wb_adr_o : std_logic_vector(31 downto 0);
    signal wb_dat_i : std_logic_vector(31 downto 0);
    signal wb_dat_o_u : std_ulogic_vector(31 downto 0);
    signal wb_dat_o : std_logic_vector(31 downto 0);
    signal wb_we_o : std_logic;
    signal wb_stb_o : std_logic;
    signal wb_cyc_o : std_logic;
    signal wb_ack_i : std_logic;

    signal gpio_dummy : std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);
	signal gpio_dir : std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);

    component wb_periph_conn
        port (
            wb_rst : in std_logic;
            wb_clk : in std_logic;
            wb_adr : in std_logic_vector(31 downto 0);
            wb_we : in std_logic;
            wb_stb : in std_logic;
            wb_cyc : in std_logic;
            wb_dat_from_master : in std_logic_vector(31 downto 0);
            wb_dat_to_master : out std_logic_vector(31 downto 0);
            wb_ack : out std_logic;

            gpio : inout std_logic_vector(NUM_GPIO_CORES*32-1 downto 0);
			dir_o : out std_logic_vector(NUM_GPIO_CORES*32-1 downto 0)
        );
    end component;
begin
    rstn <= ext_reset;
    rst <= not rstn;

	led <= led_int;

	sw_conn : for i in sw'low to sw'high generate
        sw_int(i) <= sw(i) when gpio_dir(i + 47) = '0' else 'Z';
	end generate;

    wb_adr_o <= std_logic_vector(wb_adr_o_u);
    wb_dat_o <= std_logic_vector(wb_dat_o_u);

    core_pll_inst : entity work.core_pll
    port map (
        CLK => clk_50mhz,
        CLKOP => clk,
        LOCK => open
    );

    neorv32_top_inst: neorv32_top
    generic map (
        CUSTOM_ID                    => x"10000000",
        -- General --
        CLOCK_FREQUENCY              => 40000000,   -- clock frequency of clk_i in Hz
        INT_BOOTLOADER_EN            => true,              -- boot configuration: true = boot explicit bootloader; false = boot from int/ext (I)MEM
        -- RISC-V CPU Extensions --
        CPU_EXTENSION_RISCV_C        => true,              -- implement compressed extension?
        CPU_EXTENSION_RISCV_M        => true,              -- implement mul/div extension?
        CPU_EXTENSION_RISCV_Zicsr    => true,              -- implement CSR system?
        CPU_EXTENSION_RISCV_Zicntr   => true,              -- implement base counters?
        -- Internal Instruction memory --
        MEM_INT_IMEM_EN              => true,              -- implement processor-internal instruction memory
        MEM_INT_IMEM_SIZE            => 8*1024, -- size of processor-internal instruction memory in bytes
        -- Internal Data memory --
        MEM_INT_DMEM_EN              => true,              -- implement processor-internal data memory
        MEM_INT_DMEM_SIZE            => 2*1024, -- size of processor-internal data memory in bytes
        -- Processor peripherals --
        IO_MTIME_EN                  => true,              -- implement machine system timer (MTIME)?
        IO_GPIO_EN                   => false,
        IO_UART0_EN                  => true,              -- implement primary universal asynchronous receiver/transmitter (UART0)?
        FAST_MUL_EN                  => true,  -- use DSPs for M extension's multiplier

        MEM_EXT_EN                   => true
    )
    port map (
        -- Global control --
        clk_i       => clk,       -- global clock, rising edge
        rstn_i      => rstn,      -- global reset, low-active, async

        -- primary UART0 (available if IO_UART0_EN = true) --
        uart0_txd_o => tx, -- UART0 send data
        uart0_rxd_i => rx,  -- UART0 receive data

        -- Wishbone bus interface
        wb_tag_o => open, -- request tag
        wb_adr_o => wb_adr_o_u, -- address
        wb_dat_i => std_ulogic_vector(wb_dat_i), -- read data
        wb_dat_o => wb_dat_o_u, -- write data
        wb_we_o => wb_we_o, -- read/write
        wb_sel_o => open, -- byte enable
        wb_stb_o => wb_stb_o, -- strobe
        wb_cyc_o => wb_cyc_o, -- valid cycle
        wb_ack_i => wb_ack_i, -- transfer acknowledge
        wb_err_i => open -- transfer error
    );

    wb_periph_conn_inst : wb_periph_conn
    port map (
        wb_rst => rst,
        wb_clk => clk,
        wb_adr => wb_adr_o,
        wb_we => wb_we_o,
        wb_stb => wb_stb_o,
        wb_cyc => wb_cyc_o,
        wb_dat_from_master => wb_dat_o,
        wb_dat_to_master => wb_dat_i,
        wb_ack => wb_ack_i,

        gpio(38 downto 0) => exp_io,
        gpio(46 downto 39) => led_int,
        gpio(54 downto 47) => sw_int,
        gpio(NUM_GPIO_CORES*32-1 downto 55) => gpio_dummy(NUM_GPIO_CORES*32-1 downto 55),

        dir_o => gpio_dir
    );

end architecture;
