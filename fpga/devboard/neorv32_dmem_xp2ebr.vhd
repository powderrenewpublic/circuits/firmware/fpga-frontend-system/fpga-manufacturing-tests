-- DMEM based on block ram ip.
-- The default implementation uses at least four EBR blocks
-- The ip uses a minimum of one EBR block

-- NOTE: make sure that the size of the ram ip matches the MEM_INT_DMEM_SIZE generic in neorv32_top instantiation

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library neorv32;
use neorv32.neorv32_package.all;

architecture neorv32_dmem_rtl of neorv32_dmem is
    signal addr_match : std_logic;
    signal we : std_logic;
	signal data_o_logic : std_logic_vector(31 downto 0);
    signal rden : std_logic;
    --signal ben_swap : std_logic_vector(3 downto 0);

    component dmem_ip
        port (Clock: in  std_logic; ClockEn: in  std_logic; 
            Reset: in  std_logic; ByteEn: in  std_logic_vector(3 downto 0); 
            WE: in  std_logic; Address: in  std_logic_vector(8 downto 0); 
            Data: in  std_logic_vector(31 downto 0); 
            Q: out  std_logic_vector(31 downto 0));
    end component;
begin
    addr_match <= '1' when (DMEM_BASE(31 downto 11) = addr_i(31 downto 11)) else '0';
    we <= wren_i and addr_match;
	
	data_o <= to_stdulogicvector(data_o_logic) when (rden = '1') else (others => '0');
    --ben_swap <= ben_i(0) & ben_i(1) & ben_i(2) & ben_i(3);

    bus_ack : process(clk_i)
    begin
        if rising_edge(clk_i) then
            ack_o <= addr_match and (rden_i or wren_i);
            rden <= addr_match and rden_i;
        end if;
    end process;

	dmem_ip_inst : dmem_ip
	port map (
		Clock => clk_i,
		ClockEn => '1',
		Reset => '0',
        --ByteEn => ben_swap,
        ByteEn => to_stdlogicvector(ben_i),
        WE => we,
        Address => to_stdlogicvector(addr_i(10 downto 2)),
        Data => to_stdlogicvector(data_i),
        Q => data_o_logic
	);
end;
