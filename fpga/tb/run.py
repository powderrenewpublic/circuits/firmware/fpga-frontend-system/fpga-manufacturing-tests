from vunit import VUnit

vu = VUnit.from_argv()
lib = vu.add_library('lib')

lib.add_source_files('../fpga_main_package.vhd')
lib.add_source_files('../gpio_32_wb.vhd')
lib.add_source_files('../wb_spi_wrapper.vhd')
lib.add_source_files('../wb_i2c_wrapper.vhd')
lib.add_source_files('../wb_periph_conn.vhd')

lib.add_source_files('*.vhd')

neorv32 = vu.add_library('neorv32')
neorv32.add_source_files('../neorv32/rtl/core/*.vhd')

vu.main()
