library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity gpio_32_wb_tb is
    generic (
        runner_cfg : string
    );
end gpio_32_wb_tb;

architecture tb of gpio_32_wb_tb is
    signal rst : std_logic := '1';
    signal clk : std_logic := '0';

    signal wb_adr : std_logic_vector(31 downto 0) := (others => '0');
    signal wb_we : std_logic := '0';
    signal wb_stb : std_logic := '0';
    signal wb_cyc : std_logic := '0';
    signal wb_dat_from_master : std_logic_vector(31 downto 0);
    signal wb_dat_to_master : std_logic_vector(31 downto 0);
    signal wb_ack : std_logic;

    signal gpio : std_logic_vector(31 downto 0);
begin
    test_runner : process
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("ack_test") then
                wait until rising_edge(clk);
                rst <= '0';
                wb_cyc <= '1';
                wb_stb <= '1';
                wait until rising_edge(clk);
                wait until rising_edge(clk);
                assert(wb_ack = '1');

            elsif run("write_read_test") then
                gpio <= x"2222ZZZZ";
                wait until rising_edge(clk);

                rst <= '0';
                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '1';
                wb_dat_from_master <= x"0000FFFF";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"00000008";
                wb_dat_from_master <= x"33333333";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '0';
                wb_adr <= x"00000000";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master = x"0000FFFF");

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"00000004";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master = x"22223333");

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"00000008";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master = x"33333333");
            end if;
        end loop;

        test_runner_cleanup(runner);
    end process;

    clk <= not clk after 5 ns;

    gpio_32_wb_inst : entity work.gpio_32_wb
    port map (
        gpio_io => gpio,

        rst_i => rst,
        clk_i => clk,
        adr_i => wb_adr(3 downto 0),
        dat_i => wb_dat_from_master,
        dat_o => wb_dat_to_master,
        we_i => wb_we,
        stb_i => wb_stb,
        ack_o => wb_ack,
        cyc_i => wb_cyc
    );
end architecture;
