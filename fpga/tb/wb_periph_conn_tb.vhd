library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

use work.fpga_main_package.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity wb_periph_conn_tb is
    generic (
        runner_cfg : string
    );
end wb_periph_conn_tb;

architecture tb of wb_periph_conn_tb is
    signal rst : std_logic := '1';
    signal clk : std_logic := '0';

    signal wb_adr : std_logic_vector(31 downto 0) := (others => '0');
    signal wb_we : std_logic := '0';
    signal wb_stb : std_logic := '0';
    signal wb_cyc : std_logic := '0';
    signal wb_dat_from_master : std_logic_vector(31 downto 0);
    signal wb_dat_to_master : std_logic_vector(31 downto 0);
    signal wb_ack : std_logic;

    signal gpio : std_logic_vector(383 downto 0);

    signal spi_sck : std_logic_vector(NUM_SPI_CORES-1 downto 0);
    signal spi_sdo : std_logic_vector(NUM_SPI_CORES-1 downto 0);
    signal spi_sdi : std_logic_vector(NUM_SPI_CORES-1 downto 0);
    signal spi_csn : std_logic_vector(NUM_SPI_CORES-1 downto 0);

    signal i2c_sda : std_logic_vector(NUM_I2C_CORES-1 downto 0) := (others => 'Z');
    signal i2c_scl : std_logic_vector(NUM_I2C_CORES-1 downto 0) := (others => 'Z');
begin
    test_runner : process
        variable active : boolean := true;
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("spi_test") then
                wait until rising_edge(clk);

                rst <= '0';
                wait until rising_edge(clk);

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '1';
                wb_adr <= x"A0000100";
                wb_dat_from_master <= x"00000100";
                wait until rising_edge(clk);

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"A0000104";
                wb_dat_from_master <= x"00000055";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '0';
                wb_adr <= x"A0000100";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master(31) = '1');

                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '0');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '1');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '0');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '1');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '0');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '1');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '0');
                wait until rising_edge(spi_sck(0));
                assert(spi_sdo(0) = '1');
            elsif run("gpio_write_read_test") then
                gpio(31 downto 0) <= x"2222ZZZZ";
                wait until rising_edge(clk);

                rst <= '0';
                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '1';
                wb_adr <= x"A0000000";
                wb_dat_from_master <= x"0000FFFF";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"A0000008";
                wb_dat_from_master <= x"33333333";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '0';
                wb_adr <= x"A0000000";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master = x"0000FFFF");

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"A0000004";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master = x"22223333");
                assert(gpio(31 downto 0) = x"22223333");

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"A0000008";
                wait until rising_edge(clk);

                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master = x"33333333");
            elsif run("i2c_test") then
                wait until rising_edge(clk);

                rst <= '0';
                wait until rising_edge(clk);

                -- Setup control register
                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '1';
                wb_adr <= x"A0000200";
                wb_dat_from_master <= x"A0000003";
                wait until rising_edge(clk);

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');

                -- Send 0x55
                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"A0000204";
                wb_dat_from_master <= x"00000055";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');

                -- Read control register
                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '0';
                wb_adr <= x"A0000200";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');
                -- Check that busy flag is set
                assert(wb_dat_to_master(31) = '1');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = '0');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = 'Z');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = '0');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = 'Z');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = '0');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = 'Z');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = '0');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                assert(i2c_sda(0) = 'Z');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                assert(i2c_sda(0) = 'Z');

                i2c_sda(0) <= '0';

                wait on i2c_scl(0);
                assert(i2c_scl(0) = 'Z');

                wait on i2c_scl(0);
                assert(i2c_scl(0) = '0');
                i2c_sda(0) <= 'Z';

                -- Read control register
                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '0';
                wb_adr <= x"A0000200";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');
                -- Check that i2c ack was seen
                assert(wb_dat_to_master(30) = '1');
            end if;
        end loop;

        test_runner_cleanup(runner);
    end process;

    clk <= not clk after 5 ns;

    wb_periph_conn_inst : entity work.wb_periph_conn
    port map (
        wb_clk => clk,
        wb_rst => rst,

        wb_adr => wb_adr,
        wb_we => wb_we,
        wb_stb => wb_stb,
        wb_cyc => wb_cyc,
        wb_dat_from_master => wb_dat_from_master,
        wb_dat_to_master => wb_dat_to_master,
        wb_ack => wb_ack,

        gpio => gpio,

        spi_sck => spi_sck,
        spi_sdo => spi_sdo,
        spi_sdi => spi_sdi,
        spi_csn => spi_csn,

        i2c_sda => i2c_sda,
        i2c_scl => i2c_scl
    );
end architecture;
