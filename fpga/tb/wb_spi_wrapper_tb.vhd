library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library vunit_lib;
context vunit_lib.vunit_context;

entity wb_spi_wrapper_tb is
    generic (
        runner_cfg : string
    );
end wb_spi_wrapper_tb;

architecture tb of wb_spi_wrapper_tb is
    signal rst : std_logic := '1';
    signal clk : std_logic := '0';

    signal wb_adr : std_logic_vector(31 downto 0) := (others => '0');
    signal wb_we : std_logic := '0';
    signal wb_stb : std_logic := '0';
    signal wb_cyc : std_logic := '0';
    signal wb_dat_from_master : std_logic_vector(31 downto 0);
    signal wb_dat_to_master : std_logic_vector(31 downto 0);
    signal wb_ack : std_logic;

    signal clkgen : std_logic_vector(7 downto 0);

    signal spi_sck_o : std_logic;
    signal spi_sdo_o : std_logic;
    signal spi_sdi_i : std_logic := '0';
    signal spi_csn_o : std_logic_vector(7 downto 0);

    signal irq_o : std_logic;
begin
    test_runner : process
        variable active : boolean := true;
    begin
        test_runner_setup(runner, runner_cfg);

        while test_suite loop
            if run("basic") then
                wait until rising_edge(clk);

                rst <= '0';
                wait until rising_edge(clk);

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '1';
                wb_adr <= x"00000000";
                wb_dat_from_master <= x"00000100";
                wait until rising_edge(clk);

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_adr <= x"00000004";
                wb_dat_from_master <= x"00000055";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');

                wb_cyc <= '1';
                wb_stb <= '1';
                wb_we <= '0';
                wb_adr <= x"00000000";
                wait until rising_edge(clk);
                assert(wb_ack = '0');

                wait until rising_edge(clk);

                wb_cyc <= '0';
                wb_stb <= '0';
                wait until rising_edge(clk);
                assert(wb_ack = '1');
                assert(wb_dat_to_master(31) = '1');

                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '0');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '1');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '0');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '1');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '0');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '1');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '0');
                wait until rising_edge(spi_sck_o);
                assert(spi_sdo_o = '1');
            end if;
        end loop;

        test_runner_cleanup(runner);
    end process;

    clkgen_proc : process(clk)
    begin
        if rising_edge(clk) then
            if rst = '1' then
                clkgen <= (others => '0');
            else
                clkgen <= std_logic_vector(unsigned(clkgen) + 1);
            end if;
        end if;
    end process;

    clk <= not clk after 5 ns;

    wb_spi_wrapper_inst : entity work.wb_spi_wrapper
    port map (
        clk => clk,
        rst => rst,

        wb_adr => wb_adr(2 downto 2),
        wb_we => wb_we,
        wb_stb => wb_stb,
        wb_cyc => wb_cyc,
        wb_dat_from_master => wb_dat_from_master,
        wb_dat_to_master => wb_dat_to_master,
        wb_ack => wb_ack,

        clkgen_i => clkgen,

        spi_sck_o => spi_sck_o,
        spi_sdo_o => spi_sdo_o,
        spi_sdi_i => spi_sdi_i,
        spi_csn_o => spi_csn_o,

        irq_o => irq_o
    );
end architecture;
