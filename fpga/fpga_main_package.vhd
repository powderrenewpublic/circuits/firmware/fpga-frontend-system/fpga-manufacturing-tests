library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package fpga_main_package is
    constant NUM_GPIO_CORES : natural := 12;
    constant NUM_SPI_CORES : natural := 4;
    constant NUM_I2C_CORES : natural := 5;

    constant NUM_GPIO_PINS : natural := 315;
end fpga_main_package;
