import sexpdata
import re

'''
Takes in a netlist from the fpga_main kicad schematic and turns it into pin
constraints for the Lattice Diamond lpf file.
'''

def rewrite_net_name(net_name):
    # \ -> _
    # Voltage (2.5V) -> 2V5
    tokens = net_name[1:].split('/')
    net_name = '_'.join(tokens[-2:])
    net_name = re.sub(r'2_5|2\.5V' , '2V5', net_name)
    net_name = re.sub(r'3_3|3\.3V' , '3V3', net_name)
    net_name = re.sub(r' |\.', '_', net_name)
    net_name = re.sub(r'\{|\}|\~', '', net_name)
    net_name = re.sub(r'(\d+$)', r'[\1]', net_name)
    net_name = re.sub(r'\-', r'_', net_name)
    return net_name

def make_lpf(pin, net_name):
    return f'LOCATE COMP "{net_name}" SITE "{pin[1]}"; #{pin[4]}'

def make_top(pin):
    net_name = rewrite_net_name(pin[4])
    return f'{net_name} : out std_logic;'

net_list_path = 'fpga_main.net'

net_list_file = open('fpga_main.net')

data = sexpdata.load(net_list_file)

for item in data:
    if not isinstance(item, list):
        continue

    if item[0] == sexpdata.Symbol('nets'):
        nets = item

pins = []
for net in sexpdata.cdr(nets):
    name = net[2][1]

    for node in sexpdata.cdr(net):
        if sexpdata.car(node) != sexpdata.Symbol('node'):
            continue

        ref = None
        pin = None
        pinfunction = None
        pintype = None

        for attribute in sexpdata.cdr(node):
            if sexpdata.car(attribute) == sexpdata.Symbol('ref'):
                ref = attribute[1]
            if sexpdata.car(attribute) == sexpdata.Symbol('pin'):
                pin = attribute[1]
            if sexpdata.car(attribute) == sexpdata.Symbol('pinfunction'):
                pinfunction = attribute[1]
            if sexpdata.car(attribute) == sexpdata.Symbol('pintype'):
                pintype = attribute[1]

        ignore_pintypes = ['power_in', 'no_connect']
        if ref == 'U201' and not pintype in ignore_pintypes:
            pins.append((ref, pin, pinfunction, pintype, name))

ignore = [r'JTAG']

common = [(r'REF_IN', 'Clock_Source_REF_IN'), (r'FPGA_OSC', 'ROSC_OUT')]#, 'SCL', 'SDA']

busses = []
busses.append((r'SDA$|POWER_SOCKET_GPIO_A1', 'I2C_SDA'))
busses.append((r'SCL$|POWER_SOCKET_GPIO_A0', 'I2C_SCL'))
busses.append((r'\{CS\}|LE$', 'SPI_CS'))
busses.append((r'SCLK$|PLL\.CLK', 'SPI_SCLK'))
busses.append((r'SDI$|DIN$|PLL\.DATA', 'SPI_MOSI'))
busses.append((r'SDO$|DOUT$|PLL\.MUXOUT', 'SPI_MISO'))
busses.append((r'DEBUG', 'FPGA_DEBUG_GPIO'))
bus_cts = [0] * len(busses)

differential = [(r'A_3_3\.3_', 'EXT_CLK_IN')]

gpio_ct = 0
for pin in pins:
    ignore_flag = False
    for net in ignore:
        if re.search(net, pin[4]):
            ignore_flag = True
            break

    if ignore_flag:
        continue

    bus_flag = False
    for i in range(len(busses)):
        if re.search(busses[i][0], pin[4]):
            print(make_lpf(pin, f'{busses[i][1]}[{bus_cts[i]}]'))
            bus_cts[i] += 1
            bus_flag = True
            break

    if bus_flag:
        continue

    differential_flag = False
    for net in differential:
        if re.search(net[0], pin[4]):
            differential_flag = True
            if re.search(r'_N$', pin[4]):
                break

            print(make_lpf(pin, net[1]))
            print(f'IOBUF PORT "{net[1]}" IO_TYPE=LVCMOS33D ;')

    if differential_flag:
        continue

    common_flag = False
    for net in common:
        if re.search(net[0], pin[4]):
            print(make_lpf(pin, net[1]))
            common_flag = True
            break

    if common_flag:
        continue

    if pin[3] == 'bidirectional':
        print(make_lpf(pin, f'gpio[{gpio_ct}]'))
        gpio_ct += 1
    else:
        print(make_lpf(pin, rewrite_net_name(pin[4])))
