import argparse
import re

parser = argparse.ArgumentParser(
    prog="Pin Data Lookup",
    description=\
"""Lookup in the FPGA LPF constraint file information on the GPIO pins.

Do not save the output in the repo as the LPF file may change from time to
time.""",)

parser.add_argument('lpf_filename', nargs="?",
                    default="../fpga/fpga_manufacturing_tests.lpf",
                    help="Filename/path to input mapping file")
action = parser.add_mutually_exclusive_group(required=True)
action.add_argument('--pin-index', type=int, help="Index of a pin to lookup")
action.add_argument('--list', default=False, action="store_true",
                    help="Print out the list of all GPIOs")
action.add_argument('--search', type=str,
                    help="Find pin with net name including <search>")
action.add_argument('--regexp', type=str,
                    help="Find pin with net name matching <regexp>")

args = parser.parse_args()


gpio_re = re.compile(r"LOCATE COMP \"gpio\[(?P<index>\d+)\]\" SITE \"[^\"]*\""
                     r"; #(?P<net_name>.*)$")

pin_map = {}

with open(args.lpf_filename, 'r') as lpf_file:
    for line in lpf_file:
        gpio_match = gpio_re.match(line)
        if gpio_match:
            index = int(gpio_match.groupdict()["index"])
            net_name = gpio_match.groupdict()["net_name"]
            assert index not in pin_map
            pin_map[index] = net_name


def print_pin(index, net_name):
    print(f"{index:4d}: {net_name}")


if args.pin_index is not None:
    print_pin(args.pin_index, net_name)
    quit()

if args.list:
    for pin_index, net_name in sorted(pin_map.items()):
        print_pin(pin_index, net_name)
    quit()

if args.search is not None:
    for pin_index, net_name in sorted(pin_map.items()):
        if args.search in net_name:
            print_pin(pin_index, net_name)
    quit()

if args.regexp is not None:
    arg_re = re.compile(args.regexp)
    for pin_index, net_name in sorted(pin_map.items()):
        if arg_re.search(net_name):
            print_pin(pin_index, net_name)
    quit()
