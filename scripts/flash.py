import serial
import argparse
import time

parser = argparse.ArgumentParser(description='Flash the neorv32 firmware')
parser.add_argument('-p', metavar='serial_port')
parser.add_argument('-f', metavar='firmware binary')

args = parser.parse_args()

ser = serial.Serial(args.p, 19200, timeout=1)

if not ser.isOpen():
    print('opening')
    ser.open()

firmware = open(args.f, 'rb')

print(ser.read())
ser.write('t'.encode())
ser.flush()
print(ser.read(1000))
ser.write('u'.encode())
ser.flush()
time.sleep(0.1)
print(ser.read(1000))

ser.write(firmware.read())
ser.flush()

print(ser.read(1000))

ser.write('e'.encode())
ser.flush()

ser.close()
firmware.close()
