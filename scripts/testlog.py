#!/usr/bin/python
import serial
import argparse
import time
from datetime import datetime
import os

parser = argparse.ArgumentParser(description='Flash the neorv32 firmware and capture logs')
parser.add_argument('-p', metavar='serial_port')
parser.add_argument('-f', metavar='firmware_binary')

args = parser.parse_args()

ser = serial.Serial( args.p if args.p else "/dev/ttyUSB1", 19200, timeout=1)

if not ser.isOpen():
    print('opening')
    ser.open()

firmware = open( args.f if args.f else "neorv32_exe.bin", 'rb')

print(ser.read())
ser.write('t'.encode())
ser.flush()
print(ser.read(1000))
ser.write('u'.encode())
ser.flush()
time.sleep(0.1)
print(ser.read(1000))

ser.write(firmware.read())
ser.flush()

print(ser.read(1000))

ser.write('e'.encode())
ser.flush()

logfile = open( "current.log", "w" )
print( "Log recorded " + datetime.now().ctime(), file=logfile )

address = None
started = False

while True:
    l = ser.readline().decode().rstrip( "\r\n" )
    print( l )
    if len( l ) > 1 and l[ 0 ] == "@":
        if l[ 1 ] == 'a':
            address = l[ 2: ]
        elif l[ 1 ] == 's':
            started = True
        elif l[ 1 ] == 'x':
            break
    elif started:
        print( l, file=logfile )

if( address ):
    os.rename( "current.log", address + ".log" )

ser.close()
firmware.close()
