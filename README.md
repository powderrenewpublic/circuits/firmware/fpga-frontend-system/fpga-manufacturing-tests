#Setup

Run: `git submodule init`

#Directory Structure

    fpga-manufacturing-tests           - Project root folder
    |
    |-fpga                             - Lattice Diamond Project and VHDL Source
    | |-tb                             - Testbenches
    | |-neorv32                        - Git submodule for the NEORV32 project
    | | |-sw
    | | | |-common                     - Makefile, linker script, startup assembly (for reference)
    | | | |-lib                        - NEORV32 specific drivers
    | | | |-docs                       - NEORV32 documentation
    | |-devboard                       - Lattice Diamond Project and source for breiva2 devboard
    |
    |-scripts                          - Utility Scripts
    |
    |-software                         - Software for the NEORV32 soft core
    | |-lib                            - Common drivers for peripherals
    | |-fpgatest                       - Main testing program
